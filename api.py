from fastapi.responses import HTMLResponse
from fastapi import FastAPI, UploadFile, Request
import uvicorn
import json
import requests
from fastapi.responses import JSONResponse
from database.db_connection import DBConnection
from mail.mail import Mail
from fastapi_utils.tasks import repeat_every
from datetime import datetime


app = FastAPI()

with open('configuration/config.json') as f:
    config = json.load(f)
    wait_time = config['wait_time']
    port = config['port']
    host = config['host']


@app.post("/upload/")
async def upload(request: Request):
    '''It reads the contents of the file, opens the file, loads the file into a json object, and then
    closes the file

    Parameters
    ----------
    file : UploadFile
        UploadFile

    Returns
    -------
        The return value is a dictionary with a key of "message" and a value of "Successfully uploaded
    {file.filename}".

    '''
    json_data = await request.json()
    mail = Mail.from_dict(dict(json_data))
    cursor = DBConnection().connection.cursor()
    cursor.execute(
        f'INSERT INTO mail (from_addr, from_password, to_addr, subj, body, post_date, send_date) VALUES ("{mail.from_addr}", "{mail.password}", "{mail.to_addr}", "{mail.subject}", "{mail.body}", "{datetime.now().strftime("%y/%m/%d %H:%M:%S")}", "{mail.send_date}")')
    info = 'Mail added to database with id ' + str(cursor.lastrowid) + " !"
    print(info)
    return JSONResponse(content={'message': info})


@app.get('/get_info/{id}')
async def get_info(id: int):
    '''It connects to the database, executes a query, and returns the result

    Parameters
    ----------
    id : int
        The id of the mail you want to get the info of.

    Returns
    -------
        A tuple of the data in the row.

    '''
    cursor = DBConnection().connection.cursor()
    cursor.execute(f'SELECT * FROM mail WHERE id = {id}')
    return cursor.fetchone()


@app.get('/get_all_info')
async def get_all_info():
    '''It connects to the database and returns all the rows in the mail table.

    '''
    cursor = DBConnection().connection.cursor()
    cursor.execute('SELECT * FROM mail')
    return cursor.fetchall()


@app.on_event("startup")
@repeat_every(seconds=wait_time)
def check_send_dates():
    '''It checks if the send date of a mail is in the past and if so, it sends the mail

    '''
    print('Checking send dates...')
    cursor = DBConnection().connection.cursor()
    cursor.execute('SELECT * FROM mail')
    mails = [list(elt) for elt in cursor.fetchall()]
    for mail in mails:
        if mail[-1] == 'WAITING':
            if datetime.strptime(mail[7], "%y/%m/%d %H:%M:%S") <= datetime.now():
                print("Sending mail n°" + str(mail[0]) + "...")
                send_mail(mail[0])
                cursor = DBConnection().connection.cursor()
                cursor.execute(
                    f'UPDATE mail SET send_state = "SENT" WHERE id = {mail[0]}')
                DBConnection().connection.commit()


def send_mail(id: int):
    '''It gets a row from the database, creates a Mail object from it, and sends it

    '''
    cursor = DBConnection().connection.cursor()
    cursor.execute(f'SELECT * FROM mail WHERE id = {id}')
    mail_row = cursor.fetchone()
    mail = Mail.from_db(mail_row)
    mail.send()


if __name__ == '__main__':
    uvicorn.run(app, port=port, host=host)
