from datetime import datetime
import smtplib
from email.mime.text import MIMEText


class Mail:
    def __init__(self, from_addr, password, to_addr, subject, body, send_date=None):
        '''The function takes in a from_addr, to_addr, subject, body, attachments, id, and send_date. 

        The function then sets the id, from_addr, to_addr, subject, body, and attachments to the values passed in.
        The function then sets the post_date to the current date and time.
        If the send_date is None, then the function sets the send_date to the current date and time. 
        If the send_date is not None, then the function sets the send_date to the value passed in.

        Parameters
        ----------
        from_addr
            The email address of the sender.
        password
            The password of the sender.
        to_addr
            The email address of the recipient.
        subject
            The subject of the email
        body
            The body of the email.
        send_date
            The date the email should be sent.

        Example
        -------
        >>> mail = Mail("a@a.a", "aPassword", "b@b.b", "A subject", "A body")
        >>> print(mail.send_date == mail.post_date == datetime.now())
        True
        '''
        self.id = id
        self.password = password
        self.from_addr = from_addr
        self.to_addr = to_addr
        self.subject = subject
        self.body = body

        self.post_date = datetime.now().strftime("%y/%m/%d %H:%M:%S")
        if send_date == None:
            self.send_date = datetime.now().strftime("%y/%m/%d %H:%M:%S")
        else:
            self.send_date = send_date

    def __str__(self):
        '''It returns a string representation of the object

        Returns
        -------
            A string representation of the object.

        Example
        -------
        >>> mail = Mail("a@a.a", "aPassword", "b@b.b", "A subject", "A body", send_date = datetime(2020, 1, 1, 0, 0, 0))
        >>> print(mail)
        Mail object :
        | from: a@a.a
        | to: b@b.b
        | subject: A subject
        | sending date: 2020-01-01 00:00:00
        '''
        return "Mail object :" + \
            "\n| from: " + self.from_addr + \
            "\n| to: " + self.to_addr + \
            "\n| subject: " + self.subject + \
            "\n| sending date: " + str(self.send_date)

    def from_dict(dict):
        '''It returns a Mail object from a dictionary.

        Parameters
        ----------
        dict
            The dictionary to convert.

        Returns
        -------
            A Mail object.
        '''
        return Mail(dict['from_addr'], dict['password'],
                    dict['to_addr'], dict['subject'], dict['body'],
                    dict['send_date'] if 'send_date' in dict else None)

    def from_db(db_row):
        '''It returns a Mail object from a database row.

        Parameters
        ----------
        db_row
            The database row to convert.

        Returns
        -------
            A Mail object.
        '''
        return Mail(db_row[1], db_row[2], db_row[3], db_row[4], db_row[5], db_row[6])

    def send(self):
        '''It sends an email to the address specified in the to_addr variable.

        '''
        msg = MIMEText(self.body)
        msg['Subject'] = self.subject
        msg['To'] = self.to_addr
        smtp_server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        smtp_server.login(self.from_addr, self.password)
        smtp_server.sendmail(self.from_addr, [self.to_addr], msg.as_string())
        smtp_server.quit()


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=False)
