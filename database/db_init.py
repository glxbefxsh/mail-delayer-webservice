import os
import dotenv
from database.db_connection import DBConnection


class DBInit:
    def __init__(self):
        with open('database/db_init.sql') as sql_file,\
                DBConnection().connection as connection:
            cursor = connection.cursor()
            sql_script = sql_file.read()
            cursor.executescript(sql_script)
            connection.commit()
