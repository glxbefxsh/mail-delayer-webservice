DROP TABLE IF EXISTS mail;
CREATE TABLE mail (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    from_addr TEXT NOT NULL,
    from_password TEXT NOT NULL,
    to_addr TEXT NOT NULL,
    subj TEXT NOT NULL,
    body TEXT NOT NULL,
    post_date TEXT NOT NULL,
    send_date TEXT NOT NULL,
    send_state TEXT DEFAULT 'WAITING'
);

