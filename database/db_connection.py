import sqlite3 as sl
from utils.singleton import Singleton


class DBConnection(metaclass=Singleton):
    """
    Technical class to open only one connection to the DB.
    """

    def __init__(self):
        self.__connection = sl.connect(
            'database/database.db', check_same_thread=False)

    @property
    def connection(self):
        '''This function returns the connection object.

        Returns
        -------
            The connection object.
        '''
        return self.__connection
