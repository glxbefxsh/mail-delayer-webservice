import tkinter as tk
import tkinter as tk
import json
from mail.mail import Mail
from datetime import datetime


def get_dict():
    email_info = {}
    email_info['sender_address'] = sender_entry.get()
    email_info['sender_password'] = password_entry.get()
    email_info['destination_address'] = destination_entry.get()
    email_info['subject'] = subject_entry.get()
    email_info['body'] = body_entry.get('1.0', 'end')
    try:
        email_info['send_date'] = datetime.strptime(
            date_entry.get(), '%m/%d/%y %H:%M:%S')
    except:
        email_info['send_date'] = None
    return email_info


def save_json():
    email_info = get_dict()

    with open('client/your_email.json', 'w') as f:
        json.dump(email_info, f)

    root.destroy()


root = tk.Tk()
root.title('Email App')

sender_label = tk.Label(root, text='Sender Address:')
sender_label.grid(row=0, column=0, padx=5, pady=5)
sender_entry = tk.Entry(root)
sender_entry.grid(row=0, column=1, padx=5, pady=5, sticky="nsew")

password_label = tk.Label(root, text='Sender Password:')
password_label.grid(row=1, column=0, padx=5, pady=5)
password_entry = tk.Entry(root, show='*')
password_entry.grid(row=1, column=1, padx=5, pady=5, sticky="nsew")

destination_label = tk.Label(root, text='Destination Address:')
destination_label.grid(row=2, column=0, padx=5, pady=5)
destination_entry = tk.Entry(root)
destination_entry.grid(row=2, column=1, padx=5, pady=5, sticky="nsew")

subject_label = tk.Label(root, text='Subject:')
subject_label.grid(row=3, column=0, padx=5, pady=5)
subject_entry = tk.Entry(root)
subject_entry.grid(row=3, column=1, padx=5, pady=5, sticky="nsew")

body_label = tk.Label(root, text='Body:')
body_label.grid(row=4, column=0, padx=5, pady=5)
body_entry = tk.Text(root, width=30, height=10)
body_entry.grid(row=4, column=1, padx=5, pady=5, sticky="nsew")

date_label = tk.Label(root, text='Send Date:')
date_label.grid(row=5, column=0, padx=5, pady=5)
date_entry = tk.Entry(root)
date_entry.grid(row=5, column=1, padx=5, pady=5, sticky="nsew")

save_button = tk.Button(root, text='Save Json', command=save_json)
save_button.grid(row=6, column=1, padx=5, pady=5, sticky="w")

root.mainloop()
