import json
import requests
import os

with open('configuration/config.json') as f:
    config = json.load(f)
    wait_time = config['wait_time']
    port = config['port']
    host = config['host']

url = 'http://' + host + ':' + str(port) + '/upload/'
folder = os.getcwd()
filename = folder + '\\client\\example_mail.json'
with open(filename, 'r') as f:
    myobj = json.load(f)

print(requests.post(url, json=myobj))
