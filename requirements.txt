fastapi==0.95.0
fastapi_utils==0.2.1
python-dotenv==1.0.0
requests==2.28.1
uvicorn==0.21.1
