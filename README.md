## Presentation 

__Mail Delay Webservice__ is a Python package that allows to launch an API that can send emails __from Gmail only__ at anytime. This package also provides examples of use of the API, as well as a __Json Maker tool__, that allows you to save your emails as a ready to post ```.json``` file.

The emails are stored into a SQLite database, and sent via Gmail SMTP.

## Usage

#### 1. Using Docker

__Building & Running a Docker container :__
After cloning this repository on your computer, open a terminal inside of the repository foler, and type following commands :

```bash
docker build . -t mail_delay_webservice
docker run --name mail_delay_container -d -i -t mail_delay_webservice /bin/sh
```

The first command will build a container using the ```Dockerfile```, and the second one will run this container.

__Starting the API :__
To launch the API, use the following command :
```bash
docker exec mail_delay_container python api.py
```

__Testing the API :__
On a __separate terminal__, use the following command to run ```client_test.py```, in order to test the API : 
```bash
docker exec mail_delay_container python client/client_test.py
```
This will post the ```example_mail.json``` file located in the ```client``` folder, which looks like this :
```json
{
    "from_addr": "maildelaytest@gmail.com",
    "password": "eskjxilynjqlkgek",
    "to_addr": "maildelaytest@gmail.com",
    "subject": "Test Subject",
    "body": "Test Body"
}
```
 If you want the test email to be sent to your email address, you can change the ```to_addr``` parameter. You can also add a ```send_date``` parameter under format ```yyyy/mm/dd HH:MM:SS``` (for example ```2020/01/01 00:00:00```).

 Without a ```send_date``` parameter, the email will be sent immediately (the first time the API checks send dates, which might take some time if ```wait_time``` is long).

 __Using the API :__
Along with the post request used in the ```client_test.py``` script, the API provides two other requests :

 - ```GET``` on ```/get_info/{id}```, which displays information about mail n°```id```.
  - ```GET``` on ```/get_all_info/```, which displays information about all mails.

To write a custom email, you can use the __Json Maker tool__ using ```client/json_maker.py``` to save an email under adapted Json format.


#### 2. Using your Python distribution

__Installing required packages :__
Start by installing required Python packages using the following command :
```bash
pip install -r requirements
```

__Starting the API :__
To launch the API, use the following command inside of the repository folder :
```bash
python api.py
```

__Testing the API :__
On a __separate terminal__, use the following command to run ```client_test.py```, in order to test the API : 
```bash
docker exec mail_delay_container python client/client_test.py
```
This will post the ```example_mail.json``` file located in the ```client``` folder, which looks like this :
```json
{
    "from_addr": "maildelaytest@gmail.com",
    "password": "eskjxilynjqlkgek",
    "to_addr": "maildelaytest@gmail.com",
    "subject": "Test Subject",
    "body": "Test Body"
}
```
 If you want the test email to be sent to your email address, you can change the ```to_addr``` parameter. You can also add a ```send_date``` parameter under format ```yyyy/mm/dd HH:MM:SS``` (for example ```2020/01/01 00:00:00```).

 Without a ```send_date``` parameter, the email will be sent immediately (the first time the API checks send dates, which might take some time if ```wait_time``` is long).

 __Using the API :__
Along with the post request used in the ```client_test.py``` script, the API provides two other requests :

 - ```GET``` on ```/get_info/{id}```, which displays information about mail n°```id```.
  - ```GET``` on ```/get_all_info/```, which displays information about all mails.

  
To write a custom email, you can use the __Json Maker tool__ using ```client/json_maker.py``` to save an email under adapted Json format.

## Configuration

To configure the package, you can edit ```configuration/config.json```. Here, you can set the time period of the mail sending process. For example, if you choose ```wait_time = 60```, the API will check each minute if a mail send date has been reached, and if so, it will send it.
The two other parameters are the host and port on which the API will run. 
Here is the default ```config.json``` file :
```json
{
    "wait_time": 15,
    "host": "0.0.0.0",
    "port": 8080
}
```

## Architecture diagramm

![alt text](https://gitlab.com/glxbefxsh/mail-delayer-webservice/-/raw/main/archi.png)