FROM python:3.8-slim-buster

COPY requirements.txt requirements.txt
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip3 install -r requirements.txt
ADD . /usr/local/
WORKDIR /usr/local/
# ENTRYPOINT ["python3", "api.py"] ?